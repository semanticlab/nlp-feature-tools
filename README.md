# nlp-feature-tools

This library provides tools for handling and generating NLP features from textual content.

## Subpackages

* [semanticlab-dawg](https://gitlab.com/semanticlab/nlp-feature-tools/tree/master/semanticlab-dawg) - Directed Acyclic Word Graphs (DAWG) for efficient pattern search and extraction.
* [semanticlab-ngram](https://gitlab.com/semanticlab/nlp-feature-tools/tree/master/semanticlab-ngram) - Efficient extraction of ngrams based on part-of-speech grammar groups.
