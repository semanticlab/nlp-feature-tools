package net.semanticlab.nlp.ngram.helper.sentence;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.document.Document;
import com.weblyzard.api.model.document.partition.CharSpan;
import com.weblyzard.api.model.document.partition.DocumentPartition;
import com.weblyzard.api.model.document.partition.TokenCharSpan;

import net.semanticlab.nlp.ngram.helper.annotation.AnnotationHelperInterface;

class SentenceFragmentIteratorTest {

    @Test
    void testSentenceFragmentIterator() {
        Map<DocumentPartition, List<Span>> partitions = new HashMap<>();
        partitions.put(DocumentPartition.TOKEN,
                List.of(new TokenCharSpan(10, 15, null, null),
                        new TokenCharSpan(16, 20, null, null),
                        new TokenCharSpan(22, 29, null, null)));
        Document document = new Document().setPartitions(partitions);

        assertEquals(List.of(new TokenCharSpan(10, 15, null, null),
                new TokenCharSpan(16, 20, null, null),
                new TokenCharSpan(22, 29, null, null)),
                getTokenSequence(document, new CharSpan(10, 29)));
        assertEquals(List.of(new TokenCharSpan(10, 15, null, null),
                new TokenCharSpan(16, 20, null, null)),
                getTokenSequence(document, new CharSpan(9, 25)));
        assertEquals(List.of(new TokenCharSpan(16, 20, null, null)),
                getTokenSequence(document, new CharSpan(11, 25)));
        assertEquals(Collections.emptyList(),
                getTokenSequence(document, new CharSpan(11, 12)));
        assertEquals(Collections.emptyList(),
                getTokenSequence(document, new CharSpan(23, 30)));
    }
    

    private static List<TokenCharSpan> getTokenSequence(Document document, Span sentence) {
        document.getPartitions().put(DocumentPartition.SENTENCE, Collections.singletonList(sentence));
        SentenceFragmentIterator s = new SentenceFragmentIterator(document, AnnotationHelperInterface.getAnnotationHelper(document));
        return s.hasNext() ? s.next() : Collections.emptyList();
    }
}
