package net.semanticlab.nlp.ngram.helper.sentence;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.document.Document;
import com.weblyzard.api.model.document.partition.CharSpan;
import com.weblyzard.api.model.document.partition.DocumentPartition;

class SentenceFilterTest {
    
    @Test
    void testSentenceFilter() {
        SentenceFilter f = new TitleFilter();
        Document d = new Document();
        Span sentence = new CharSpan(10, 20);
        
        // test no title partition
        assertFalse(f.filterSentence(d, sentence));

        // with title partition
        d.setPartitions(Map.of(DocumentPartition.TITLE, Collections.singletonList(new CharSpan(21, 25))));
        assertFalse(f.filterSentence(d, sentence));
        assertTrue(f.filterSentence(d, new CharSpan(20, 21)));
    }

    @Test
    void testOverlap() {
        Span sentence = new CharSpan(10, 20);

        final Map<String, CharSpan> TEST_CASES = Map.of(
                // @formatter:off
                "identical", new CharSpan(10, 20),
                "larger", new CharSpan(5, 25),
                "smaller", new CharSpan(12, 18),
                "leftOverlap", new CharSpan(8, 10),
                "rightOverlap", new CharSpan(20, 22)
                // @formatter:on
                );

        for (Map.Entry<String, CharSpan> testCase: TEST_CASES.entrySet()) {
            assertTrue(TitleFilter.overlap(sentence, testCase.getValue()), testCase.getKey());
        }

        // no overlap
        assertFalse(TitleFilter.overlap(new CharSpan(8, 9), sentence));
        assertFalse(TitleFilter.overlap(new CharSpan(21, 21), sentence));
    }
}
