package net.semanticlab.nlp.ngram;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import com.google.common.collect.ImmutableMap;
import com.weblyzard.api.model.annotation.Annotation;
import com.weblyzard.api.model.document.Document;
import net.semanticlab.nlp.ngram.helper.annotation.AnnotationNGramAssembly;

/**
 * Test the document based on the following document
 * 
 * <pre>
 * 
 * The/DT Spirit|NN of/IN the/DT Sovereign|NNP Lord|NNP is/VBZ on/IN me/PRP ,/, because/IN
 * the/DT Lord|NNP has/VBZ anointed/VBN me/PRP to/TO proclaim/VB good/JJ news|NN to/TO the/DT
 * poor/JJ ./.
 * 
 * He/PRP has/VBZ sent/VBN me/PRP to/TO bind/VB up/RP the/DT brokenhearted/VBN ,/, to/TO proclaim/VB
 * freedom|NN for/IN the/DT captives|NNS and/CC release|NN from/IN darkness|NN for/IN the/DT
 * prisoners|NNS ,/, to/TO proclaim/VB the/DT year|NN of/IN the/DT Lord|NNP 's/POS favor|NN
 * and/CC the/DT day|NN of/IN vengeance|NN of/IN our/PRP$ God|NNP ,/, to/TO comfort/VB all/DT who/WP
 * mourn/VBP ,/, and/CC provide/VB for/IN those/DT who/WP grieve/VBP in/IN Zion|NNP -/: to/TO
 * bestow/VB on/IN them/PRP a/DT crown|NN of/IN beauty|NN instead/RB of/IN ashes|NNS ,/, the/DT
 * oil|NN of/IN joy|NN instead/RB of/IN mourning|NN ,/, and/CC a/DT garment|NN of/IN praise|NN
 * instead/RB of/IN a/DT spirit|NN of/IN despair|NN ./.
 * 
 * They/PRP will/MD be/VB called/VBN oaks|NNS of/IN righteousness|NN ,/, a/DT planting|NN of/IN
 * the/DT Lord|NNP for/IN the/DT display|NN of/IN his/PRP$ splendor|NN ./. """
 * 
 * </pre>
 * 
 * @author Albert Weichselbraun
 *
 */

class DocumentTokenizerTest {

    private final static Document TEST_DOCUMENT =
            TestHelper.readJson("Jeremia-61.json", Document.class);
    private final static Map<String, String> POS_GRAMMAR_MAPPING =
            ImmutableMap.<String, String>builder().put("NN", "noun").put("NNP", "noun")
                    .put("NNS", "noun").put("NNPS", "noun").put("NE", "noun").put("JJ", "adj")
                    .put("JJS", "adj").put("JJR", "adj").put("RB", "adv").put("RBS", "adv")
                    .put("RBR", "adv").put("IN", "prep").build();

    private static final List<String> UNIGRAM_NOUN_PATTERN = List.of("noun");
    private static final List<String> BIGRAM_NOUN_PATTERN = List.of("noun:noun");
    private static final List<String> UNIGRAM_BIGRAM_NOUN_PATTERN = List.of("noun", "noun:noun");

    private static final List<String> UNIGRAM_REFERENCE_NGRAMS = List.of("Spirit|NN",
            "Sovereign|NNP", "Lord|NNP", "Lord|NNP", "news|NN", "freedom|NN", "captives|NNS",
            "release|NN", "darkness|NN", "prisoners|NNS", "year|NN", "Lord|NNP", "favor|NN",
            "day|NN", "vengeance|NN", "God|NNP", "Zion|NNP", "crown|NN", "beauty|NN", "ashes|NNS",
            "oil|NN", "joy|NN", "mourning|NN", "garment|NN", "praise|NN", "spirit|NN", "despair|NN",
            "oaks|NNS", "righteousness|NN", "planting|NN", "Lord|NNP", "display|NN", "splendor|NN");
    private static final List<String> BIGRAM_REFERENCE_NGRAMS = List.of("Sovereign Lord|NNP NNP");
    private static final List<String> UNIGRAM_BIGRAM_REFERENCE_NGRAMS =
            List.of("Spirit|NN", "Sovereign|NNP", "Sovereign Lord|NNP NNP", "Lord|NNP", "Lord|NNP",
                    "news|NN", "freedom|NN", "captives|NNS", "release|NN", "darkness|NN",
                    "prisoners|NNS", "year|NN", "Lord|NNP", "favor|NN", "day|NN", "vengeance|NN",
                    "God|NNP", "Zion|NNP", "crown|NN", "beauty|NN", "ashes|NNS", "oil|NN", "joy|NN",
                    "mourning|NN", "garment|NN", "praise|NN", "spirit|NN", "despair|NN", "oaks|NNS",
                    "righteousness|NN", "planting|NN", "Lord|NNP", "display|NN", "splendor|NN");

    private static final List<String> COMPLEX_GRAMMAR_PATTERN =
            List.of("noun", "noun:noun", "noun:prep:noun", "adj:noun");
    private static final List<String> COMPLEX_REFERENCE_NGRAMS = List.of("Spirit|NN",
            "Sovereign|NNP", "Sovereign Lord|NNP NNP", "Lord|NNP", "Lord|NNP", "good news|JJ NN",
            "news|NN", "freedom|NN", "captives|NNS", "release|NN", "release from darkness|NN IN NN",
            "darkness|NN", "prisoners|NNS", "year|NN", "Lord|NNP", "favor|NN", "day|NN",
            "day of vengeance|NN IN NN", "vengeance|NN", "God|NNP", "Zion|NNP", "crown|NN",
            "crown of beauty|NN IN NN", "beauty|NN", "ashes|NNS", "oil|NN", "oil of joy|NN IN NN",
            "joy|NN", "mourning|NN", "garment|NN", "garment of praise|NN IN NN", "praise|NN",
            "spirit|NN", "spirit of despair|NN IN NN", "despair|NN", "oaks|NNS",
            "oaks of righteousness|NNS IN NN", "righteousness|NN", "planting|NN", "Lord|NNP",
            "display|NN", "splendor|NN");

    private static final List<Annotation> ANNOTATIONS =
            List.of(Annotation.build("/year_of_the_Lord's_favor/").setStart(254).setEnd(278),
                    Annotation.build("/Zion/").setStart(378).setEnd(382),
                    Annotation.build("/The_Spirit_of_the_Sovereign_Lord/").setStart(0).setEnd(32),
                    Annotation.build("/spirit_of_despair/").setStart(512).setEnd(529),
                    Annotation.build("/spirit_of_despair./").setStart(512).setEnd(530));
    private static final List<String> COMPLEX_AND_ANNOTATION_REFERENCE_NGRAMS = List.of(
            "year of the Lord\u2019s favor|NE", "Zion|NE", "The Spirit of the Sovereign Lord|NE",
            "spirit of despair|NE", "spirit of despair.|NE", "Lord|NNP", "good news|JJ NN",
            "news|NN", "freedom|NN", "captives|NNS", "release|NN", "release from darkness|NN IN NN",
            "darkness|NN", "prisoners|NNS", "day|NN", "day of vengeance|NN IN NN", "vengeance|NN",
            "God|NNP", "crown|NN", "crown of beauty|NN IN NN", "beauty|NN", "ashes|NNS", "oil|NN",
            "oil of joy|NN IN NN", "joy|NN", "mourning|NN", "garment|NN",
            "garment of praise|NN IN NN", "praise|NN", "oaks|NNS",
            "oaks of righteousness|NNS IN NN", "righteousness|NN", "planting|NN", "Lord|NNP",
            "display|NN", "splendor|NN");
    private static final List<String> COMPLEX_AND_ANNOTATION_KEY_REFERENCE_NGRAMS =
            List.of("key:/year_of_the_Lord's_favor/|NE", "key:/Zion/|NE",
                    "key:/The_Spirit_of_the_Sovereign_Lord/|NE", "key:/spirit_of_despair/|NE",
                    "key:/spirit_of_despair./|NE", "Lord|NNP", "good news|JJ NN", "news|NN",
                    "freedom|NN", "captives|NNS", "release|NN", "release from darkness|NN IN NN",
                    "darkness|NN", "prisoners|NNS", "day|NN", "day of vengeance|NN IN NN",
                    "vengeance|NN", "God|NNP", "crown|NN", "crown of beauty|NN IN NN", "beauty|NN",
                    "ashes|NNS", "oil|NN", "oil of joy|NN IN NN", "joy|NN", "mourning|NN",
                    "garment|NN", "garment of praise|NN IN NN", "praise|NN", "oaks|NNS",
                    "oaks of righteousness|NNS IN NN", "righteousness|NN", "planting|NN",
                    "Lord|NNP", "display|NN", "splendor|NN");

    @Test
    void testUnigramTokenize() {
        DocumentTokenizer d = new DocumentTokenizer(POS_GRAMMAR_MAPPING, UNIGRAM_NOUN_PATTERN,
                Collections.emptyList(), AnnotationNGramAssembly.getSurfaceFormAssembly(), false);
        List<String> tokens = getNgramStrings(d.tokenize(TEST_DOCUMENT));
        assertEquals(UNIGRAM_REFERENCE_NGRAMS, tokens);
    }

    @Test
    void testBigramTokenize() {
        DocumentTokenizer d = new DocumentTokenizer(POS_GRAMMAR_MAPPING, BIGRAM_NOUN_PATTERN,
                Collections.emptyList(), AnnotationNGramAssembly.getSurfaceFormAssembly(), false);
        List<String> tokens = getNgramStrings(d.tokenize(TEST_DOCUMENT));
        assertEquals(BIGRAM_REFERENCE_NGRAMS, tokens);
    }

    @Test
    void testUnigramBigramTokenize() {
        DocumentTokenizer d = new DocumentTokenizer(POS_GRAMMAR_MAPPING,
                UNIGRAM_BIGRAM_NOUN_PATTERN, Collections.emptyList(),
                AnnotationNGramAssembly.getSurfaceFormAssembly(), false);
        List<String> tokens = getNgramStrings(d.tokenize(TEST_DOCUMENT));
        assertEquals(UNIGRAM_BIGRAM_REFERENCE_NGRAMS, tokens);
    }

    @Test
    void testAdvancedTokenize() {
        DocumentTokenizer d = new DocumentTokenizer(POS_GRAMMAR_MAPPING, COMPLEX_GRAMMAR_PATTERN,
                Collections.emptyList(), AnnotationNGramAssembly.getSurfaceFormAssembly(), false);
        List<String> tokens = getNgramStrings(d.tokenize(TEST_DOCUMENT));
        assertEquals(COMPLEX_REFERENCE_NGRAMS, tokens);
    }

    @Test
    void testAdvancedTokenizeWithAnnotations() {
        DocumentTokenizer d = new DocumentTokenizer(POS_GRAMMAR_MAPPING, COMPLEX_GRAMMAR_PATTERN,
                Collections.emptyList(), AnnotationNGramAssembly.getSurfaceFormAssembly(), false);
        Document document = new Document().setContent(TEST_DOCUMENT.getContent())
                .setPartitions(TEST_DOCUMENT.getPartitions()).setAnnotations(ANNOTATIONS);
        List<String> tokens = getNgramStrings(d.tokenize(document));
        assertEquals(COMPLEX_AND_ANNOTATION_REFERENCE_NGRAMS, tokens);
    }

    @Test
    void testAdvancedTokenizeWithAnnotationKey() {
        DocumentTokenizer d = new DocumentTokenizer(POS_GRAMMAR_MAPPING, COMPLEX_GRAMMAR_PATTERN,
                Collections.emptyList(),
                AnnotationNGramAssembly.getEntityDescriptorAssembly("key:"), false);
        Document document = new Document().setContent(TEST_DOCUMENT.getContent())
                .setPartitions(TEST_DOCUMENT.getPartitions()).setAnnotations(ANNOTATIONS);
        List<String> tokens = getNgramStrings(d.tokenize(document));
        assertEquals(COMPLEX_AND_ANNOTATION_KEY_REFERENCE_NGRAMS, tokens);
    }

    @Test
    void testEmptySentenceDocument() {
        DocumentTokenizer d = new DocumentTokenizer(POS_GRAMMAR_MAPPING, COMPLEX_GRAMMAR_PATTERN,
                Collections.emptyList(), AnnotationNGramAssembly.getSurfaceFormAssembly(), false);
        Document document = new Document();
        document.setPartitions(new HashMap<>());
        assertEquals(Collections.emptyList(), d.tokenize(document));
    }

    private static List<String> getNgramStrings(List<NGram> ngrams) {
        return ngrams.stream().map(n -> n.toString()).collect(Collectors.toList());
    }

}
