package net.semanticlab.nlp.ngram.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import com.weblyzard.api.model.document.partition.TokenCharSpan;

class GrammarGroupMapperTest {

    private static final GrammarGroupMapper g = new GrammarGroupMapper(
                    Map.of("NN", "noun", "NNP", "noun", "VBG", "verb", "VB", "verb"));

    private static final TokenCharSpan NN = new TokenCharSpan().setPos("NN");
    private static final TokenCharSpan VB = new TokenCharSpan().setPos("VB");
    private static final TokenCharSpan NNP = new TokenCharSpan().setPos("NNP");
    private static final TokenCharSpan VBG = new TokenCharSpan().setPos("VBG");
    private static final TokenCharSpan XYZ = new TokenCharSpan().setPos("XYZ");

    @Test
    public void testGrammarGroupMapping() {
        assertEquals(g.getGrammarGroup(NN), g.getGrammarGroup(NNP));
        assertEquals(g.getGrammarGroup(VBG), g.getGrammarGroup(VB));
        assertNotEquals(g.getGrammarGroup(VBG), g.getGrammarGroup(NNP));

        // handling of unknown pos tags
        assertNotEquals(g.getGrammarGroup(VBG), g.getGrammarGroup(XYZ));
    }

    @Test
    public void testGetNumericalValidGrammarGroupSpec() {
        List<List<Integer>> spec = g.getNumericalValidGrammarGroupSpec(
                        List.of("noun", "noun:noun", "verb:noun"));
        assertEquals(spec.size(), 3);
    }

    @Test
    public void testGetInvalidNumericalValidGrammarGroupSpec() {
        assertThrows(IllegalArgumentException.class, () -> g.getNumericalValidGrammarGroupSpec(
                        List.of("noun", "noun:noun", "verb:noun", "unknown:verb")));
    }

    @Test
    public void testGetGrammarGroupForUnknownPosTag() {
        assertEquals(GrammarGroupMapper.NO_GRAMMAR_GROUP_DEFINED, g.getGrammarGroup(XYZ));
    }


}
