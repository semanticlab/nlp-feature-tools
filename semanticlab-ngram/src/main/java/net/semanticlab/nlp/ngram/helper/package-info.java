/**
 * Helper classes used by the {@link net.semanticlab.nlp.ngram.DocumentTokenizer}.
 */
package net.semanticlab.nlp.ngram.helper;
