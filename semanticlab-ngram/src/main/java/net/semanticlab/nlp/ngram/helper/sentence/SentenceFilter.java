package net.semanticlab.nlp.ngram.helper.sentence;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.document.Document;

/**
 * Interface used to filter sentences that should not be considered in
 * the keyword extraction process.
 * 
 * @author Albert Weichselbraun
 *
 */
public interface SentenceFilter {
    
    public boolean filterSentence(Document document, Span sentence);
    
}
