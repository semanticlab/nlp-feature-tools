package net.semanticlab.nlp.ngram.helper.sentence;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.document.Document;
import com.weblyzard.api.model.document.partition.DocumentPartition;
import com.weblyzard.api.model.document.partition.TokenCharSpan;
import lombok.extern.slf4j.Slf4j;
import net.semanticlab.nlp.ngram.helper.annotation.AnnotationHelperInterface;

/**
 * Iterates over {@link TokenCharSpan}s within a {@link Document}, considering sentences and annotations.
 * 
 * @author Albert Weichselbraun
 *
 */
@Slf4j
public class SentenceFragmentIterator implements Iterator<List<TokenCharSpan>> {

    private final Document document;
    private final List<TokenCharSpan> documentTokens;
    private final Iterator<Span> documentSentences;
    private final AnnotationHelperInterface annotations;
    private final SentenceFilter sentenceFilter;
    private int currentPos = 0;
    private Span currentSentence;
    private List<TokenCharSpan> nextResult;

    
    public SentenceFragmentIterator(Document document, AnnotationHelperInterface annotationHelper) {
        this(document, annotationHelper, NoFilter.getFilter());

    }
    
    @SuppressWarnings("unchecked")
    public SentenceFragmentIterator(Document document, AnnotationHelperInterface annotationHelper, SentenceFilter sentenceFilter) {
        if (!document.getPartitions().containsKey(DocumentPartition.TOKEN)) {
            log.warn("Document '{}' does not contain any token annotations.", document.getId());
            throw new IllegalArgumentException("Document does not contain any token annotations.");
        }
        if (!document.getPartitions().containsKey(DocumentPartition.SENTENCE) || document.getPartitions().get(DocumentPartition.SENTENCE).isEmpty()) {
            log.warn("Document '{}' does not contain any sentence annotations.", document.getId());
            throw new IllegalArgumentException("Document does not contain any sentence annotations.");
        }
        this.document = document;
        this.annotations = annotationHelper;
        this.sentenceFilter = sentenceFilter;

        documentTokens = (List<TokenCharSpan>) (List<?>) document
                .getPartitions().get(DocumentPartition.TOKEN);
        documentSentences = document.getPartitions().get(DocumentPartition.SENTENCE).iterator();
        currentSentence = documentSentences.next();

        nextResult = getNextTokenSequence();
    }

    /**
     * Transforms a document into the corresponding sequence of {@link TokenCharSpan}s.
     * 
     * @param document the document to transform
     * @return a list of {@link TokenCharSpan}s
     */
    protected List<TokenCharSpan> getNextTokenSequence() {
        TokenCharSpan s;
        
        if (currentPos >= documentTokens.size()) {
            return null;
        }

        // forward to the next sentence, if required
        while (documentTokens.get(currentPos).getStart() > currentSentence.getEnd() || sentenceFilter.filterSentence(document, currentSentence)) {
            if (!documentSentences.hasNext()) {
                return null;
            }
            currentSentence = documentSentences.next();
        }


        // move to the start of the sentence (skipping annotations)
        while (currentPos < documentTokens.size()) {
            s = documentTokens.get(currentPos);
            if (s.getStart() >= currentSentence.getStart() && !annotations.isInAnnotation(s)) {
                break;
            }
            currentPos++;
        }
        int sentenceFragmentStartIndex = currentPos;

        // collect tokens until the end of the sentence is reached
        while (currentPos < documentTokens.size()) {
            s = documentTokens.get(currentPos);
            if (s.getEnd() > currentSentence.getEnd() || annotations.isInAnnotation(s)) {
                break;
            }
            currentPos++;
        }
        if (currentPos > sentenceFragmentStartIndex) {
            return documentTokens.subList(sentenceFragmentStartIndex, currentPos); 
        } 
        currentPos++;
        return getNextTokenSequence();
    }

    @Override
    public boolean hasNext() {
        return nextResult != null;
    }

    @Override
    public List<TokenCharSpan> next() {
        if (nextResult == null) {
            throw new NoSuchElementException();
        }
        List<TokenCharSpan> result = nextResult;
        nextResult = getNextTokenSequence();
        return result;
    }

}

