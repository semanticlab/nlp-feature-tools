package net.semanticlab.nlp.ngram.helper.sentence;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.document.Document;

public class NoFilter implements SentenceFilter {

    private static final SentenceFilter NO_FILTER = new NoFilter();
    
    public static SentenceFilter getFilter() {
        return NO_FILTER;
    }

    @Override
    public boolean filterSentence(Document document, Span sentence) {
        return false;
    }
}
