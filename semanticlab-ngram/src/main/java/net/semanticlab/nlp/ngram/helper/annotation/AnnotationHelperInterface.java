package net.semanticlab.nlp.ngram.helper.annotation;

import java.util.List;
import java.util.function.BiFunction;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.annotation.Annotation;
import com.weblyzard.api.model.document.Document;
import net.semanticlab.nlp.ngram.NGram;

public interface AnnotationHelperInterface {

    /**
     * Returns an {@link AnnotationHelper} for the given {@link Document}.
     */
    public static AnnotationHelperInterface getAnnotationHelper(Document document) {
        return document.getAnnotations() == null || document.getAnnotations().isEmpty()
                        ? EmptyAnnotationHelper.getEmptyAnnotationHelper()
                        : new AnnotationHelper(document);
    }

    /**
     * Returns a list of {@link NGram}s for the {@link Annotation}s within the given document.
     */
    public List<NGram> getAnnotationNgrams(BiFunction<Annotation, Document, NGram> ngramAssembly);

    /**
     * Returns true if the given {@link Span} overlaps with an {@link Annotation}.
     */
    public boolean isInAnnotation(Span s);

}
