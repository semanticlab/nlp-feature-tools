package net.semanticlab.nlp.ngram;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import com.weblyzard.api.model.document.partition.TokenCharSpan;
import lombok.AllArgsConstructor;
import net.semanticlab.nlp.dawg.DawgAllMatchIndicator;
import net.semanticlab.nlp.dawg.DawgAllMatchIndicator.MatchResult;
import net.semanticlab.nlp.dawg.node.DawgNodeInterface;
import net.semanticlab.nlp.ngram.transform.NGramTransformer;

/**
 * Extracts tokens from {@link Document}s based on patterns encoded in {@link DawgNodeInterface}s.
 * 
 * @author Albert Weichselbraun
 *
 */
@AllArgsConstructor
public class TokenExtractor {

    private final DawgNodeInterface<Integer> rootNode;
    private final Function<TokenCharSpan, Integer> indexLookupFunction;
    private final int rank;
    private final List<NGramTransformer> ngramTransformers;

    /**
     * Extract all matches from the given haystack.
     */
    public List<NGram> extractAllMatches(String content, List<TokenCharSpan> haystack) {
        List<NGram> result = new ArrayList<>(3 * haystack.size());
        for (int i = 0; i < haystack.size(); i++) {
            DawgAllMatchIndicator<Integer, TokenCharSpan> nodeMatcher =
                            new DawgAllMatchIndicator<>(rootNode, indexLookupFunction);
            for (int j = i; j < i + rank && j < haystack.size(); j++) {
                MatchResult match = nodeMatcher.matches(haystack.get(j));
                if (match == MatchResult.MATCHES) {
                    NGram ngram = extractNgram(content, haystack, i, j);
                    // apply all available filters to the ngram
                    if (ngramTransformers.stream()
                                    .allMatch(transformer -> transformer.transform(ngram))) {
                        result.add(ngram);
                    }
                } else if (match == MatchResult.BREAK) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Extracts the given NGram from the content.
     */
    private static NGram extractNgram(String content, List<TokenCharSpan> haystack, int startPos,
                    int endPos) {
        int ngramLen = endPos - startPos + 1;
        String[] tokens = new String[ngramLen];
        String[] poss = new String[ngramLen];
        for (int i = 0; i < ngramLen; i++) {
            TokenCharSpan currentSpan = haystack.get(startPos + i);
            tokens[i] = content.substring(currentSpan.getStart(), currentSpan.getEnd());
            poss[i] = currentSpan.getPos();
        }
        return new NGram(tokens, poss);
    }

}
