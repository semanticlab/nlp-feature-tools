package net.semanticlab.nlp.ngram;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NGram {

    final String posDelimiter = "|";

    /* list of ngram terms (string) */
    private String[] terms;

    /* list of part-of-speech (POS) descriptors (string) */
    private String[] poss;


    @Override
    public String toString() {
        if (poss.length == terms.length) {
            return String.join(posDelimiter, String.join(" ", terms), String.join(" ", poss));
        }
        return String.join(" ", terms); // fallback
    }
}
