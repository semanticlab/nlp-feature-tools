package net.semanticlab.nlp.ngram.helper.annotation;

import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.annotation.Annotation;
import com.weblyzard.api.model.document.Document;
import lombok.NoArgsConstructor;
import net.semanticlab.nlp.ngram.NGram;

/**
 * An efficient {@link AnnotationHelperInterface} implementation for documents that do not contain
 * any annotations.
 * 
 * @author Albert Weichselbraun
 *
 */
@NoArgsConstructor
public class EmptyAnnotationHelper implements AnnotationHelperInterface {
    
    private static final AnnotationHelperInterface EMPTY_ANNOTATION_HELPER = new EmptyAnnotationHelper();

    public static AnnotationHelperInterface getEmptyAnnotationHelper() {
        return EMPTY_ANNOTATION_HELPER;
    }


    @Override
    public List<NGram> getAnnotationNgrams(BiFunction<Annotation, Document, NGram> ngramAssembly) {
        return Collections.emptyList();
    }

    @Override
    public boolean isInAnnotation(Span s) {
        return false;
    }

}
