package net.semanticlab.nlp.ngram.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import com.weblyzard.api.model.document.partition.TokenCharSpan;
import net.semanticlab.nlp.ngram.DocumentTokenizer;

/**
 * Provides translations of part-of-speech (POS) tags and grammar patterns to the corresponding
 * numeric patterns used by the {@link DocumentTokenizer}.
 * 
 * @author Albert Weichselbraun
 *
 */
public class GrammarGroupMapper {

    public static final Integer NO_GRAMMAR_GROUP_DEFINED = Integer.valueOf(-1);
    public static final String GRAMMAR_GROUP_SEPARATOR = ":";

    /** A mapping of POS tags to the corresponding grammar group identifier. */
    private final Map<String, Integer> posGrammarGroupIdentifierMap = new HashMap<>();

    /**
     * Initializes the {@link GrammarGroupMapper}.
     * 
     * @param mapping a mapping of POS tags to the corresponding grammar group.
     */
    public GrammarGroupMapper(Map<String, String> mapping) {
        for (Map.Entry<String, String> posGrammarMapping : mapping.entrySet()) {
            posGrammarGroupIdentifierMap.put(posGrammarMapping.getKey(),
                            posGrammarMapping.getValue().hashCode());
        }
    }

    /**
     * Returns the grammar group identifier for the given POS tag.
     */
    public Integer getGrammarGroup(TokenCharSpan token) {
        return posGrammarGroupIdentifierMap.getOrDefault(token.getPos(), NO_GRAMMAR_GROUP_DEFINED);
    }

    /**
     * Transforms the given textual specification of valid grammar group patterns into a numerical
     * one.
     * 
     * @throws IllegalArgumentException if undefined grammar groups are used.
     */
    public List<List<Integer>> getNumericalValidGrammarGroupSpec(
                    List<String> validGrammarGroupPatterns) {

        final Set<Integer> validGrammarGroups =
                        new HashSet<>(posGrammarGroupIdentifierMap.values());
        return validGrammarGroupPatterns.stream().map(grammarPattern -> Arrays
                        .stream(grammarPattern.split(GRAMMAR_GROUP_SEPARATOR)).map(grammarGroup -> {
                            int grammarGroupHash = grammarGroup.hashCode();
                            if (!validGrammarGroups.contains(grammarGroupHash)) {
                                throw new IllegalArgumentException(
                                                "Undefined grammar group '" + grammarGroup + "'.");
                            }
                            return grammarGroupHash;
                        }).collect(Collectors.toList())).collect(Collectors.toList());
    }

}
