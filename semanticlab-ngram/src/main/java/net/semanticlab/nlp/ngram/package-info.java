/**
 * N-Gram extraction from webLyzard {@link com.weblyzard.api.model.document.Document}s.
 */
package net.semanticlab.nlp.ngram;
