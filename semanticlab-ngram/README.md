# semanticlab-ngram

 Efficient extraction of ngrams based on part-of-speech grammar groups.  The tokenizer requires a [weblyzard API](https://gitlab.com/weblyzard/weblyzard_api) document (see [Document JavaDoc](http://static.javadoc.io/com.weblyzard.api/weblyzard-api/0.2.1.4/com/weblyzard/api/model/document/Document.html)) and performs tokenization based on an (optional) list of document `Annotation`s and the following `DocumentPartition`:
 
 * `DocumentPartition.TOKEN` - provides information on token positions and the token's part-of-speech (POS) tags
 * `DocumentParititon.SENTENCE` - indicates sentence borders
 
 other `DocumentParititions` might be used by helper classes such as the sentence filter (e.g. to remove title sentences from the tokenization process).
 
 
## Packages

* `net.semanticlab.nlp.ngram`: extracts ngrams based on grammar patterns from weblyzard Documents
* `net.semanticlab.nlp.ngram.transform`: modifies or filters extracted ngrams
* `net.semanticlab.nlp.ngram.helper.annotation`: provides helper classes for dealing with annotations within the Document
* `net.semanticlab.nlp.ngram.helper.sentence`: classes for filtering input sentences (e.g. by ignoring titles, etc.)

## Concepts

(1) **Pos grammar group mappings** map pos tags to arbitrary grammar groups that are then considered in the ngram extraction


Example: 

*  `NN`, `NNP`,  `NNS`, `NNPS` and `NE` to grammar group `noun`
* `JJ`, `JJS` and `JJR` to grammar group `adj`
* `RB`, `RBS` and `RBR` to group `adv` and
* `IN` to group `prep`
  	
```java
   Map<String, String> posGrammarGroupMapping  = ImmutableMap
            .<String, String>builder().put("NN", "noun").put("NNP", "noun")
            .put("NNS", "noun").put("NNPS", "noun").put("NE", "noun").put("JJ", "adj")
            .put("JJS", "adj").put("JJR", "adj").put("RB", "adv").put("RBS", "adv")
            .put("RBR", "adv").put("IN", "prep").build();

```

Grammar group names might be based on real groups (e.g. nouns, adj, adv) or on fictional group names _and_ must not contain a colon (`:`).

(2) The **list of valid grammar group patterns** specifies which patterns should be extracted from the input text.
The library considers all ngrams that match any of the given grammar patterns.

Example:
```
List<String> validGrammarGroupPatterns = List.of("noun", "noun:noun", "noun:prep:noun", "adj:noun");
```

(3) **Annotations** are not considered in the ngram extraction process, but are directly added to the result. The `AnnotationNgramAssembly` class provides `BiFunctions` for transfering these annotations to ngrams.

(4) The `NGramTransformer` interface exposes the `transform(NGram ngram)` method that is used to modify ngrams or filter them. This can be used for post-processing steps such as removing ngrams with invalid characters.

## Usage example
The `DocumentTokenizer` object extracts `NGram` from the input Document.

```java
// an optional list of ngram transfomers that
// filter and/or manipulate the extracted NGrams.
List<NGramTransformer> transformer = Collections.emptyList();

// specifies how Annotations within the document should be handled.
// Examples: 
// - include their surface form as an ngram
// - include an identifier into the list of ngrams
// - drop Annotations
BiFunction<Annotation, Document, NGram> annotationAssembly = AnnotationNgramAssembly.getSurfaceFormsAssembly();

DocumentTokenizer tokenizer = new DocumentTokenizer(posGrammarGroupMapping, 
	validGrammarGroupPatterns, transformer, annotationAssembly);

List<NGram> result = tokenizer.tokenize(inputDocument);
```

The `annotationAssembly` determines how `Annotations`will be handeled. In the example above the annotation's surface form is extracted. `AnnotationNgramAssembly`also provides other helper classes, as for instance, `AnnotationNGramAssembly.getEntityDescriptorAssembly(String prefix)`that extracts the annotation's `entityDescriptor`field instead of the surface form, as demonstrated in the example below.

```java
BiFunction<Annotation, Document, NGram> annotationAssembly = AnnotationNgramAssembly.getEntityDescriptorAssembly("annotation:");
```

The prefix defines an optional prefix that will be added to the extracted annotation. 
