/**
 * A Direct Acyclic Word Graph/Object implementation used for NGram extraction.
 * 
 * @author Albert Weichselbraun
 *
 */
package net.semanticlab.nlp.dawg;
