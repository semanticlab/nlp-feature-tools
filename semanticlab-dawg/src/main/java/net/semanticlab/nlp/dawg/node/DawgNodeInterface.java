package net.semanticlab.nlp.dawg.node;

/**
 * Functions every DAWG node needs to support.
 * 
 * @author Albert Weichselbraun
 *
 */
public interface DawgNodeInterface<T> {

    /** Returns the child for the given index. */
    public DawgNodeInterface<T> getChild(T index);

    /** Determines whether the current node has a child for the given index. */
    public boolean hasChild(T index);

    /** Add a child for the given index. */
    public DawgNodeInterface<T> addChild(T childIndex);

    /** Indicates whether the given node is a leaf node in the DAWG. */
    public boolean isLeaf();

    public void setLeaf(boolean isLeaf);
}
