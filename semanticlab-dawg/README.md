# semanticlab-dawg

Provides a generic directed acyclic word graph implementation that is optimized for information extraction.
 
## Packages

* `net.semanticlab.net.nlp.dawg`: classes for creating a DAWG and for applying it to search and information extraction tasks.
* `net.semanticlab.net.nlp.dawg.node`: interfaces and implementations of DAWG nodes.

